CC=gcc
UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
OS_ARGS=-fpic -shared
OUTEXT=.so
endif
ifeq ($(UNAME), Darwin)
OS_ARGS=-dynamiclib
OUTEXT=.dylib
endif

build:
	$(CC) -o preload$(OUTEXT) $(OS_ARGS) hound.c
