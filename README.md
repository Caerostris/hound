# hound - debug proxy reloaded

This is a small utilty that can be used to inspect socket connections.  
Work in progress.

## How does it work?

Hound is a small libarary that is loaded before a program is executed.  
It alters unix' core network functions to enable logging.
The output can either be sent to a seperate program for analysis or written to a log file.

## Features

* log dns requests

## TODO

* log dns response
* log socket connections
* log `send()` calls
* graphical interface

## Usage

### Linux

    $ git clone https://Caerostris@bitbucket.org/Caerostris/hound.git
    $ cd hound
    $ make
    $ HOUND_USE_LOG=1 LD_PRELOAD=./hound.so curl -L -O http://google.com/a
    $ cat hound.log

### OS X

    $ git clone https://Caerostris@bitbucket.org/Caerostris/hound.git
    $ cd hound
    $ make
    $ HOUND_USE_LOG=1 DYLD_FORCE_FLAT_NAMESPACE=1 DYLD_INSERT_LIBRARIES=hound.dylib curl -L -O http://google.com/a curl -L -O http://google.com/a
    $ cat hound.log
   
## Configuration

Configuration is done via environment variables.

#### HOUND\_USE\_LOG
Value: 0/1  
Default: 0  
Use a log file
`HOUND_USE_SOCKET` and `HOUND_USE_LOG` can be used at the same time.

#### HOUND\_USE\_SOCKET
Value: 0/1  
Default: 1  
Send the log to a unix socket
`HOUND_USE_SOCKET` and `HOUND_USE_LOG` can be used at the same time.

#### HOUND\_LOG\_FILE
Value: {string}  
Default: ./hound.log  
Log file path for `HOUND_USE_LOG`

#### HOUND\_SOCKET
Value: {string}
Default: /tmp/.hound
Unix socket path for `HOUND_USE_SOCKET`

## License

All code released under the Mozilla Public License 2.0
