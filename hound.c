#define _GNU_SOURCE

#include <dlfcn.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

// set load as the "main method"
void __attribute__((constructor)) load(void);

// global variables
int fd;
bool hound_use_socket;
bool hound_use_log;
char *hound_log_file;
char *hound_socket;

// populate the settings
void init_settings()
{
	char *env_use_socket;
	char *env_use_log;

	// load settings from the environment
	env_use_socket = getenv("HOUND_USE_SOCKET"); // use a unix socket? 1 or 0
	env_use_log = getenv("HOUND_USE_LOG"); // use a log file? 1 or 0
	hound_log_file = getenv("HOUND_LOG_FILE"); // path to log file
	hound_socket = getenv("HOUND_SOCKET"); // path to socket file

	// decide wether to use a unix socket or not. default: 1
	if(env_use_socket == NULL || strcmp(env_use_socket, "") == 0)
	{
		hound_use_socket = true;
	}
	else if(strcmp(env_use_socket, "0") == 0)
	{
		hound_use_socket = false;
	}

	// decide wether to use a log file or not. default: 0
	if(env_use_log == NULL || strcmp(env_use_log, "") == 0)
	{
		hound_use_log = false;
	}
	else if(strcmp(env_use_log, "1") == 0)
	{
		hound_use_log = true;
	}

	// path to log file. default: ./hound.log
	if(hound_log_file == NULL || strcmp(hound_log_file, "") == 0)
	{
		hound_log_file = "hound.log";
	}

	// path to socket file. default: /tmp/.hound
	if(hound_socket == NULL || strcmp(hound_socket, "") == 0)
	{
		hound_socket = "/tmp/.hound";
	}
}

// executed once or library has been loaded
void load(void)
{
	struct sockaddr_un addr;

	// load settings
	init_settings();

	// init message
	printf("HOUND LIBRARY LOADED\n");
	if(hound_use_socket)
	{
		printf("USING SOCKET: %s\n", hound_socket);

		// open the socket and connect to it
		fd = socket(AF_UNIX, SOCK_STREAM, 0);
		memset(&addr, 0, sizeof(addr));
		addr.sun_family = AF_UNIX;
		strncpy(addr.sun_path, hound_socket, sizeof(addr.sun_path)-1);
		connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	}
	if(hound_use_log)
	{
		printf("USING LOG: %s\n", hound_log_file);
	}
}

// logs a DNS resolve to enabled channels. (Unix socket and/or log file)
void hound_log_resolve(const char *function, const char *domain, const char *result)
{
	if(hound_use_log)
	{
		FILE *f;
		char *log_msg;

		f = fopen(hound_log_file, "a");
		fprintf(f, "%s resolved %s: %s\n", function, domain, result);
		fclose(f);
	}

	if(hound_use_socket)
	{
		char buf[13 + strlen(function) + strlen(domain) + strlen(result)];

		if(fd == -1)
		{
			printf("HOUND: Failed, socket not open");
			return;
		}

		snprintf(buf, sizeof(buf), "RESOLVE %s %s %s\r\n", function, domain, result);

		send(fd, buf, strlen(buf), 0);
	}
}

// log all calls to gethostbyname
struct hostent *gethostbyname(const char *name)
{
	// log the call
	hound_log_resolve("GETHOSTBYNAME", name, "8.8.8.8");

	// get hold of the original function
	struct hostent *(*ghbn)(const char*);
	ghbn = dlsym(RTLD_NEXT, "gethostbyname");

	// return the original function
	return (*ghbn)(name);
}


// log all calls to getaddrinfo
int getaddrinfo(const char *node, const char *service,
                const struct addrinfo *hints,
                struct addrinfo **res)
{
	// log the call
	hound_log_resolve("GETADDRINFO", node, "8.8.8.8");

	// get hold of the original function
	int (*gtaddrinf)(const char *node, const char *service,
                const struct addrinfo *hints,
                struct addrinfo **res);
	gtaddrinf = dlsym(RTLD_NEXT, "getaddrinfo");

	// return the original function
	return gtaddrinf(node, service, hints, res);
}
